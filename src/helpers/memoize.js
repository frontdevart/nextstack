/* eslint-disable */
const memoize = (fn, resolver) => {
  const cache = new Map();
  const memoized = (...args) => {
    const key = resolver ? resolver.apply(null, args) : args[0];
    if (cache.has(key)) {
      return cache.get(key);
    } else {
      const result = fn.apply(null, args);
      cache.set(key, result);
      return result;
    }
  };
  memoized.getCache = () => cache;
  return memoized;
};

export default memoize;
