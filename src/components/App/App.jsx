import { Route, Switch, Redirect } from 'react-router-dom';
import Navigation from '../Navigation';
import ServiceDetails from '../ServiceDetails';

const App = () => (
  <div className="app">
    <Navigation />
    <Switch>
      <Route exact path="/:id">
        <ServiceDetails />
      </Route>
      <Redirect to="/" />
    </Switch>
  </div>
);

export default App;
