import React from 'react';
import { useSelector } from 'react-redux';

const ServiceDetails = () => {
  const { detail, loading, error } = useSelector(
    (state) => state.serviceDetail
  );

  if (loading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <p>Something went wrong</p>;
  }

  return <div className="">{detail.navigation_title}</div>;
};

export default ServiceDetails;
