import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import NavigationItem from '../NavigationItem';
import navigationDataActions from '../../redux/actions/navigationDataActions';

const Navigation = () => {
  const dispatch = useDispatch();
  const { getNavigationData } = navigationDataActions;
  const { items, error, loading } = useSelector(
    (state) => state.navigationData
  );

  useEffect(() => {
    dispatch(getNavigationData());
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <p>Something went wrong...</p>;
  }

  return (
    <div className="navigation">
      <div className="navigation-inner">
        <div className="navigation-arrow up">Arrow</div>
        <div className="navigation-items">
          {items.map(({ id, icon, title }) => (
            <NavigationItem id={id} icon={icon} title={title} key={id} />
          ))}
        </div>
        <div className="navigation-arrow down">Arrow</div>
      </div>
    </div>
  );
};

export default Navigation;
