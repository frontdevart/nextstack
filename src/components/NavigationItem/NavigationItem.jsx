import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import navigationDataActions from '../../redux/actions/navigationDataActions';
import serviceDetailActions from '../../redux/actions/serviceDetailsActions';

const NavigationItem = ({ id, icon, title }) => {
  const { selected, items } = useSelector((state) => state.navigationData);
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const { setNavigationCurrentData } = navigationDataActions;
  const { getServiceDetail } = serviceDetailActions;
  const checkLocationId = +location.pathname.split('').splice(1).join('');

  const clickHandler = (identifier) => {
    dispatch(
      setNavigationCurrentData(items.find((item) => item.id === identifier))
    );
    history.push(`/${identifier}`);
  };

  useEffect(() => {
    if (!Number.isNaN(checkLocationId)) {
      dispatch(getServiceDetail(checkLocationId));
      dispatch(
        setNavigationCurrentData(
          items.find((item) => item.id === checkLocationId)
        )
      );
    } else {
      dispatch(getServiceDetail(items[0].id));
    }
  }, [location.pathname]);

  return (
    <div
      className={`navigation-item ${id === selected.id ? 'selected' : ''}`}
      role="presentation"
      onClick={() => clickHandler(id)}
    >
      <div className="navigation-item-image">
        <img src={`${process.env.REACT_APP_BACKEND_URL}/${icon}`} alt={title} />
      </div>
      <div className="navigation-item-title">{title}</div>
    </div>
  );
};
export default NavigationItem;
