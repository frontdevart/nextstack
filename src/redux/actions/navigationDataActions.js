import axios from 'axios';
import {
  NAVIGATION_DATA_LOADING,
  NAVIGATION_DATA_SUCCESS,
  NAVIGATION_DATA_ERROR,
  NAVIGATION_DATA_SET_CUREENT_ITEM,
} from '../types/navigationDataTypes';

const navigationDataSuccessAction = (payload) => ({
  type: NAVIGATION_DATA_SUCCESS,
  payload,
});

const navigationDataStartedAction = () => ({
  type: NAVIGATION_DATA_LOADING,
});

const navigationDataFailureAction = (error) => ({
  type: NAVIGATION_DATA_ERROR,
  payload: {
    error,
  },
});

const setNavigationCurrentDataAction = (payload) => ({
  type: NAVIGATION_DATA_SET_CUREENT_ITEM,
  payload,
});

const setNavigationCurrentData = (peyload) => (dispatch) => {
  dispatch(setNavigationCurrentDataAction(peyload));
};

const getNavigationData = () => (dispatch) => {
  dispatch(navigationDataStartedAction());
  axios
    .get('/api/services')
    .then((res) => {
      dispatch(navigationDataSuccessAction(res.data.data.items));
    })
    .catch((err) => {
      dispatch(navigationDataFailureAction(err.message));
    });
};

export default { getNavigationData, setNavigationCurrentData };
