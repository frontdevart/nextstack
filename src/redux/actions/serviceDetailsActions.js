import axios from 'axios';
import {
  SERVICE_DETAIL_SUCCESS,
  SERVICE_DETAIL_LOADING,
  SERVICE_DETAIL_ERROR,
} from '../types/serviceDetailsTypes';

const serviceDetailSuccessAction = (payload) => ({
  type: SERVICE_DETAIL_SUCCESS,
  payload,
});

const serviceDetailStartedAction = () => ({
  type: SERVICE_DETAIL_LOADING,
});

const serviceDetailFailureAction = (error) => ({
  type: SERVICE_DETAIL_ERROR,
  payload: {
    error,
  },
});

const getServiceDetail = (id) => (dispatch) => {
  dispatch(serviceDetailStartedAction(id));
  axios
    .get(`/api/services/content/${id}`)
    .then((res) => {
      dispatch(serviceDetailSuccessAction(res.data.data.items));
    })
    .catch((err) => {
      dispatch(serviceDetailFailureAction(err.message));
    });
};

export default { getServiceDetail };
