import { combineReducers } from 'redux';
import naviagtionDataReducer from './navigationDataReducer';
import serviceDetailReducer from './serviceDetailsReducers';

const rootReducer = combineReducers({
  navigationData: naviagtionDataReducer,
  serviceDetail: serviceDetailReducer,
});

export default rootReducer;
