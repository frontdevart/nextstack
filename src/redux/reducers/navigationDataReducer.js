import {
  NAVIGATION_DATA_SUCCESS,
  NAVIGATION_DATA_LOADING,
  NAVIGATION_DATA_ERROR,
  NAVIGATION_DATA_SET_CUREENT_ITEM,
} from '../types/navigationDataTypes';

const initialState = {
  loading: false,
  items: [],
  selected: {},
  error: null,
};

const naviagtionDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case NAVIGATION_DATA_LOADING:
      return {
        ...state,
        loading: true,
      };
    case NAVIGATION_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        items: [...state.items, ...action.payload],
        selected: { ...state.selected, ...action.payload[0] },
      };
    case NAVIGATION_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    case NAVIGATION_DATA_SET_CUREENT_ITEM:
      return {
        ...state,
        selected: { ...{}, ...action.payload },
      };
    default:
      return state;
  }
};

export default naviagtionDataReducer;
