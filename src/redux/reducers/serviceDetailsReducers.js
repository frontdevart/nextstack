import {
  SERVICE_DETAIL_LOADING,
  SERVICE_DETAIL_SUCCESS,
  SERVICE_DETAIL_ERROR,
} from '../types/serviceDetailsTypes';

const initialState = {
  loading: false,
  detail: {},
  error: null,
};

const serviceDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case SERVICE_DETAIL_LOADING:
      return {
        ...state,
        loading: true,
      };
    case SERVICE_DETAIL_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        detail: { ...action.payload },
      };
    case SERVICE_DETAIL_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
};

export default serviceDetailReducer;
